# Awesome Mobile Neuralnets

A curated list of neuralnets designed to be run on mobile devices. Inspired by [awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning).

If you want to contribute to this list (please do), send me a pull request or contact me [andrew@textile.io](mailto:andrew@textile.io).

## Models

| category | what | who | training | model type |
|:-------:|:-------------------------:|:----------------:|:----------:|:----------:|
| Vision | [Scene and/or Image Classification](https://developer.apple.com/machine-learning/) | Apple | no | various deep neural networks
| Vision | [Face detection iOS API](https://machinelearning.apple.com/2017/11/16/face-detection.html) | Apple | no | deep convolutional network |
| Vision | [Not hotdog](https://medium.com/@timanglade/how-hbos-silicon-valley-built-not-hotdog-with-mobile-tensorflow-keras-react-native-ef03260747f3) | Silicon valley | no | deep neural network |
| Language | [Google Translate](https://research.googleblog.com/2015/07/how-google-translate-squeezes-deep.html) | Google | no | deep neural network |
| Language | [Multi-language Handwriting](https://machinelearning.apple.com/2017/09/12/handwriting.html) | Apple | no | convolutional neural network |
| Language | [Conversational smart reply](https://research.googleblog.com/2017/02/on-device-machine-intelligence.html), [With TensorflowLite](https://research.googleblog.com/2017/11/on-device-conversational-modeling-with.html) | Google | no | semi-supervised graph |
| Voice | [Hey Siri](https://machinelearning.apple.com/2017/10/01/hey-siri.html) | Apple | no | deep neural network |

## Tools

To find tools that help train, deploy, and run neuralnets on device, see the language specific sections over at [awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning). Below are quicklinks to some mobile-specific tools, frameworks, and services.

* [CoreML](https://developer.apple.com/documentation/coreml) - integrate trained machine learning models into your app.
* [TensorFlowLite](https://developers.googleblog.com/2017/11/announcing-tensorflow-lite.html) - on-device machine learning inference with low latency and a small binary size.
* [Textile.io](https://www.textile.io/) - feature extraction for offline training and online execution of models.
* [Clarifai](https://blog.clarifai.com/introducing-clarifais-mobile-sdk-on-device-machine-learning-whether-youre-online-or-offline/) - custom computer vision models in your mobile app.
